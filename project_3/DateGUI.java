/***************************************************************
 * 
 * @author Zachary Kleiman
 * FileMenuHandler class. Used for Project3. 
 * This handles the options in the menubar. It allows the user to Quit or read a new file and have that sorted.
 */
import javax.swing.*;
import java.io.*;
import java.util.*;
import java.awt.*;
import java.util.ArrayList;

public class DateGUI extends JFrame {
    // declare this here so all methods can access
    private TextArea order_read = new TextArea();
    private TextArea order_sorted = new TextArea();

    /**
     * @param date212list         UnsortedDate212List
     * @param date212list_ordered SortedDate212List
     * 
     *                             This draws the GUI and places the unordered dates
     *                             on the left column and the ordered ones on the
     *                             right
     */
    public void initialize(ArrayList<Date212> date212list, ArrayList<Date212> date212list_ordered) {
        // Need to convert the ArrayList to an array
        Date212[] array_dates = new Date212[date212list.size()];
        array_dates = date212list.toArray(array_dates);
        Date212[] array_dates_ordered = new Date212[date212list_ordered.size()];
        array_dates_ordered = date212list_ordered.toArray(array_dates_ordered);

        // Make the GUI and set basic information
        DateGUI DateGUI1 = new DateGUI();
        DateGUI1.setSize(400, 200);
        DateGUI1.setLocation(100, 100);
        DateGUI1.createFileMenu();
        DateGUI1.setTitle("Dates");
        DateGUI1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        DateGUI1.setVisible(true);

        // Set Grid layout
        DateGUI1.setLayout(new GridLayout(1, 2));
        Container myContentPane = DateGUI1.getContentPane();

        myContentPane.add(order_read, BorderLayout.EAST);
        myContentPane.add(order_sorted, BorderLayout.WEST);
        myContentPane.add(order_read);
        myContentPane.add(order_sorted);

        // Place the dates. Unordered is in the left column and ordered is in the right
        // column
        for (int i = 0; i < date212list.size(); i++) {
            order_read.append(array_dates[i] + "\n");
        }
        for (int i = 0; i < date212list_ordered.size(); i++) {
            order_sorted.append(array_dates_ordered[i] + "\n");
        }

    }

    /**
     * 
     * 
     * This creates the FileMenu in the GUI
     */
    private void createFileMenu() {
        JMenuItem item;
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        // fmh = file menu handler
        FileMenuHandler fmh = new FileMenuHandler(this, Project3.gui_project1);

        item = new JMenuItem("Open"); // Open...
        item.addActionListener(fmh);
        fileMenu.add(item);

        fileMenu.addSeparator(); // Add a horizontal separator line

        item = new JMenuItem("Quit"); // Quit
        item.addActionListener(fmh);
        fileMenu.add(item);

        setJMenuBar(menuBar);
        menuBar.add(fileMenu);
    }

    /**
     * 
     * 
     * This updates the GUI with newly sorted dates
     */
    public void updateGUI() {
        order_read.setText(""); // clear out
        order_sorted.setText("");
        //rewrite the new lists to the GUI
        for (int i = 0; i < Project3.date212list.size(); i++) {
            order_read.append(Project3.date212list.get(i) + "\n");
        }
        for (int i = 0; i < Project3.date212list_ordered.size(); i++) {
            order_sorted.append(Project3.date212list_ordered.get(i) + "\n");
        }
    }
}

