import javax.swing.*;
import java.lang.*;
//Copied a lot of this from my Lab4 since I could reuse. I made modification where needed
public class Project0 {
 
public static void main(String[] args) {
// We have a while (true) loop so it continues until the user inputs stop.
	while (true) {
 		// This line asks the user for input by popping out a single window
		// with text input
 		//the user input is received and stored as a String which will be compared to e and E
		String inputWord = JOptionPane.showInputDialog(null, "Please enter a sentence.");
		int e_value = checker(inputWord, "e"); //call the checker method which returns the number of e's
		int E_value = checker(inputWord, "E"); //call the checker method which returns the number of E's
 		JOptionPane.showMessageDialog(null, "Number of lower case e's: " + e_value + "\n " + "Number of upper case E's: " + E_value);
	}//end while
} //main
public static int checker(String compare_word, String letter) {
	int e_count=0; //lowercase count
	int E_count=0; //uppercase count
	//use equalignorecase to catch all varying cases of stop like stop STOP StOp etc.
	if (compare_word.equalsIgnoreCase("STOP")){ 
		System.exit(0);
	} 
	//Strings are character arrays. Store the value of string[i] in a character array (string)
	//(did this cause I could then use .equals for strings, which makes my life easier and doesn't affect anything)
	//find the charAt at index i in the for loop. Store that in the String compared.
	//we then compare it to "e" and "E" and increase the relevant count.
	for (int i=0;i<compare_word.length();i++) {
		String compared = compare_word.valueOf(compare_word.charAt(i));
		if (compared.equals("e")) {
			e_count++;
		}
		if (compared.equals("E")) {
			E_count++;
		}
	}
	//We need only return once everything's been counted so this must be out of the for loop.
	//we here return the relevant value depending on what letter has been called.
	if (letter=="e") {
		return e_count;
	}
	if (letter=="E") {
		return E_count;
	}
	return 0; //default return value since nothing would've been counted.
} // checker
} // class Project0
