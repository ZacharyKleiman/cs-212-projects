/***************************************************************
 * 
 * @author Zachary Kleiman
 * IllegalDate212Exception class. Used for Project3 and Project4. 
 * This class literally just exists to throw an exception.
 */
public class IllegalDate212Exception extends IllegalArgumentException {
    /**
     * @param message - the message of the exception
     * 
     * just call super with the argument message. This extends IllegalArgumentException so that's super
     */
    public IllegalDate212Exception(String message) {
        super(message);
    }
}

