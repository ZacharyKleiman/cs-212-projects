/***************************************************************
 * 
 * @author Zachary Kleiman
 * EditMenuHander class. Used in 
 * This handles the options in the menubar. It allows the user to Quit or read a new file and have that sorted.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

public class EditMenuHandler implements ActionListener {
	JFrame jframe; // declare class vars we need to modify
	DateGUI gui_project1;

	/**
	 * @param jf Jframe
	 *            Get a Jframe and DateGUI to modify and assign it to class
	 *            variables
	 */
	public EditMenuHandler(JFrame jf, DateGUI gui) {
		jframe = jf;
		gui_project1 = gui;
	}

	/**
	 * @param event ActionEvent Gets a date from the user, we then filter out dates not from that year and print to the GUI1
	 */
	public void actionPerformed(ActionEvent event) {
		String menuName = event.getActionCommand();

		if (menuName.equals("Search")) {
			// Get a year from the user
			String year = JOptionPane.showInputDialog("Filter dates by year");
			// blank input resets to default
			boolean blank_flag = false;
			if (year.equals("")) {
				blank_flag = true;
			}
			//handle invalid input by resetting to default
			for (int i = 0; i < year.length(); i++) {
				if (year.charAt(i) < '0' || year.charAt(i) > '9') {
					blank_flag = true;
				} else {
					blank_flag = false;
					break;
				}

			}
			if (year.length() != 4) {
				blank_flag = true;
			}

			// Restore the dates from scratch
			gui_project1.updateGUI();
			if (blank_flag == false) {
				gui_project1.order_read.setText("");
				gui_project1.order_sorted.setText("");

				for (int i = 0; i < Project4.date212list.size(); i++) {
					Date212 current = Project4.date212list.get(i);
					if (current.year_return() == Integer.parseInt(year)) {
						gui_project1.order_read.append(current + "\n");
					}
				}

				Collections.sort(Project4.date212list_ordered);
				for (int i = 0; i < Project4.date212list.size(); i++) {
					Date212 current = Project4.date212list_ordered.get(i);
					if (current.year_return() == Integer.parseInt(year)) {
						gui_project1.order_sorted.append(current + "\n");
					}
				}
			}
		}
	}
}

