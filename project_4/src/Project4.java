/***************************************************************
 * 
 * @author Zachary Kleiman
 * Project4 class. Used for Project4. 
 * This reads in a file, checks the validity of the dates and sorts it.
 */
import javax.swing.*;
import java.io.*;
import java.util.*;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JTextArea;

public class Project4 {
    // declare the two arrays as belonging to the whole class so it's accessible to all methods and to other classes
    // same for dates and the GUI
    public static ArrayList<Date212> date212list = new ArrayList<Date212>();
    public static ArrayList<Date212> date212list_ordered = new ArrayList<Date212>();
    public static ArrayList<String> dates = new ArrayList<String>();
    public static DateGUI gui_project1;

    /**
     * @param args Command-line arguments constructor reads in dates from the file,
     *             checks their validity, sorts them using Collections.sort and
     *             calls DateGUI to make the GUI
     */
    public static void main(String args[]) {
        // ArrayList<String> dates = new ArrayList<String>();
        try {
            dates = inputFromFile(args[0]);
        } catch (Exception e) {
            dates = inputFromFile("dates.txt");
        }
        // check dates isn't null and length isn't 0
        if (dates == null || dates.size() == 0) {
            System.out.println("No dates in the input file.");
            return;
        }
        check_validity_and_place(dates);
        Collections.sort(date212list_ordered);

        gui_project1 = new DateGUI();
        gui_project1.initialize(date212list, date212list_ordered);
    }

    /**
     * @param filename String
     * @return parts An ArrayList containing the dates read from the file. Get every
     *         line from the file and store in the ArrayList parts
     */
    public static ArrayList<String> inputFromFile(String filename) {
        // goal is to take input from the file
        // output should be the list of dates read in
        // whether they're valid or not
        // Variables must be declared outside of try or an error occurs
        BufferedReader input;
        try {
            input = new BufferedReader(new FileReader(filename));
        } catch (IOException E) {
            System.out.println("Invalid filename:" + E.getMessage());
            return null;
        }
        String line;
        // see if reading is possible and get initial input for line for the while loop
        try {
            line = input.readLine();
        } catch (IOException E) {
            System.out.println("Failed read:" + E.getMessage());
            return null;
        }
        ArrayList<String> parts = new ArrayList<String>();
        while (line != null) {// while
            parts.addAll(Arrays.asList(line.split(",")));
            // split it with every comma and add to the array then read the next line
            try {
                line = input.readLine();
            } catch (IOException E) {
                System.out.println("Failed read:" + E.getMessage());
                return null;
            }
        }
        // close the file being read
        try {
            input.close();
        } catch (IOException E) {
            System.out.println("Failed read:" + E.getMessage());
            return null;
        }
        return parts;
    } // method inputFromFile

    /**
     * @param parts ArrayList of type String Check the validity of the dates. If
     *              valid, add to relevant lists; if not, print to the console.
     */
    public static void check_validity_and_place(ArrayList<String> parts) {
        Date212 date;
        for (int i = 0; i < parts.size(); i++) {
            // check if date is valid, if so add to the date lists
            date = new Date212(parts.get(i));
            if (date.get_is_valid() == true) {
                date212list.add(date);
                date212list_ordered.add(date);
            }
        }
    }
}

