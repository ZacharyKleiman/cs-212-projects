/***************************************************************
 * 
 * @author Zachary Kleiman
 * FileMenuHandler class. Used for Project3 and Project4. 
 * This handles the options in the menubar. It allows the user to Quit or read a new file and have that sorted.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

public class FileMenuHandler implements ActionListener {
    JFrame jframe; // declare class vars we need to modify
    DateGUI gui_project1;

    /**
     * @param jf Jframe
     *            Get a Jframe and DateGUI to modify and assign it to class
     *            variables
     */
    public FileMenuHandler(JFrame jf, DateGUI gui) {
        jframe = jf;
        gui_project1 = gui;
    }

    /**
     * @param event ActionEvent If the action is to open a file, read the file and
     *              update the dates accordingly and the GUI too. If the user
     *              presses Quit then exit the application.
     */
    public void actionPerformed(ActionEvent event) {
        String menuName = event.getActionCommand();
        JFileChooser fileChooser = new JFileChooser();
        if (menuName.equals("Open")) {

            // Show open dialog; this method does not return until the dialog is closed
            int result = fileChooser.showOpenDialog(jframe);

            if (result == JFileChooser.APPROVE_OPTION) {
                // User selected a file
                File selectedFile = fileChooser.getSelectedFile();
                String filePath = selectedFile.getAbsolutePath();
                ArrayList<String> dates_new = Project4.inputFromFile(filePath);
                // do sorting and update GUI
                Project4.date212list.clear();
                Project4.date212list_ordered.clear();
                Project4.dates = dates_new; // update the dates
                Project4.check_validity_and_place(Project4.dates);

                Collections.sort(Project4.date212list_ordered);
                gui_project1.updateGUI();
            } else if (menuName.equals("Quit"))
                System.exit(0);
        } // actionPerformed
    }
}

