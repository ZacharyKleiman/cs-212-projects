/***************************************************************
 * 
 * @author Zachary Kleiman
 * Date212 class. Used for Project1, Project2, and Project3 and Project4. 
 * Date212 creates three private int instance variables day, month, year, and a boolean is_valid, and a date_saved string
 */
import javax.swing.*;
import java.io.*;
import java.util.*;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JTextArea;

/*
Create a class called Date212 to represent a date. It will store the year, month, and day as integers (not
as a String), so you will need three private instance variables. A constructor should be provided that
takes a String (representing the date as above). Use the substring method of class String to pull out the
month, day, and year, and parse them as integers. Include a private validity checking method that
should make sure that the month and day values are legal. If it is not a valid date, print it to the console
and do not put it in the array of dates. Create a toString method in class Date212 that will return the date
in the form mm/dd/yyyy. Use this method to display the dates in the GUI.
*/
public class Date212 implements Comparable<Date212> {
    private int day = 0;
    private int month = 0;
    private int year = 0;
    private boolean is_valid;
    private String date_saved;

    /**
     * @param date Type string, date to parse for private instance variables. Calls
     *             is_it_valid() to determine if the date is valid. It also prints
     *             any invalid dates to the console after throwing an exception.
     */
    public Date212(String date) {
        is_valid = true;
        if (date.length() != 8) {
            is_valid = false;
            return;
        }
        date_saved = date;
        year = Integer.parseInt(date.substring(0, 4));
        month = Integer.parseInt(date.substring(4, 6));
        day = Integer.parseInt(date.substring(6, 8));
        is_valid = is_it_valid();
        if (is_valid == false) {
            try {
                throw new IllegalDate212Exception(" This is an illegal date:");
            } catch (Exception e) {
                System.out.println(
                        "Illegal date:" + date.substring(0, 4) + date.substring(4, 6) + date.substring(6, 8));
            }
        }
    }

    /**
     * @return Returns the value of is_valid which is private
     */
    public boolean get_is_valid() {
        return is_valid;
    }

    /**
    * @return Returns the year
    */
    public int year_return() {
        return year;
    }

    /**
     * Checks the validity of the date set in month, day, year.
     * 
     * @return False if an invalid date, true if a valid date
     */
    private boolean is_it_valid() {
        // check if value between 1 and 12 for months
        if (month < 1 || month > 12) {
            return false; // error
        }
        // check validity of day
        if (day < 1 || day > 31) {
            return false; // error
        }
        if (day > 30 && (month == 4 || month == 6 || month == 9 || month == 11)) {
            return false; // error, invalid day of month
        }
        if (day > 31 && (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10
                || month == 12)) {
            return false;
        }
        // check february
        if (day > 28 && (month == 2)) {
            // check if it's a leap year
            if (year % 4 == 0) {
                if (day > 29) {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * Create a toString method in class Date212 that will return the date in the
     * form mm/dd/yyyy. Use this method to display the dates in the GUI.
     * 
     * @return The string sorted properly
     */
    // Create a toString method in class Date212 that will return the date in the
    // form mm/dd/yyyy. Use this method to display the dates in the GUI.
    public String toString() {
        return date_saved.substring(4, 6) + "/" + date_saved.substring(6, 8) + "/" + date_saved.substring(0, 4);
    }

    /**
     * Compare this to another Date212 object to see which one comes first -1 this
     * is less 1 this is more 0 this is equal
     * 
     * @param Date1 - type Date212
     * @return -1 if the date being compared is greater than self, 0 if equal to
     *         self, 1 if less than self
     */
    // compare this to another Date212 object to see which one comes first
    public int compareTo(Date212 Date1) {
        // Compare year, if they're equal, then check the month, then the day. Only do
        // the needed checks.
        if (this.year < Date1.year) {
            return -1;
        } else if (this.year > Date1.year) {
            return 1;
        } else {
            if (this.month < Date1.month) {
                return -1;
            } else if (this.month > Date1.month) {
                return 1;
            } else {
                if (this.day < Date1.day) {
                    return -1;
                } else if (this.day > Date1.day) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }
    }
}

