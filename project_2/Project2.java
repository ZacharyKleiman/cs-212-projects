
import javax.swing.*;
import java.io.*;
import java.util.*;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JTextArea;
import java.awt.*


public class Project2 {
    // Declare the two arrays as belonging to the whole class so it's accessible to all methods
    private static UnsortedDate212List date212list = new UnsortedDate212List();
    private static SortedDate212List date212list_ordered = new SortedDate212List();

    /**
     * @param args[]
     * constructor
     * reads in dates from the file, checks their validity and calls DateGUI to make the GUI
     */
    public static void main(String args[]) {
        ArrayList<String> dates = new ArrayList<String>();
        // Set default file if none received
        try {
            dates = inputFromFile(args[0]);
        } catch (Exception e) {
            dates = inputFromFile("dates.txt");
        }
        // Check if dates aren't null and their length isn't 0
        if (dates == null || dates.size() == 0) {
            System.out.println("No dates in the input file.");
            return;
        }
        // Check validity of dates and call GUI
        check_validity_and_place(dates);
        // Selection_sort_date();
        // Put in the unsorted dates
        // Unordered is done
        // Now to organize the ordered list
        DateGUI gui_project1 = new DateGUI();
        gui_project1.initialize(date212list, date212list_ordered);
    }

    /**
     * @param String filename
     * @return ArrayList<String> parts
     * Get every line from the file
     */
    public static ArrayList<String> inputFromFile(String filename) {
        // Goal is to take input from a file
        // Output should be the list of dates read in,
        // whether they're valid or not.
        // Variables must be declared outside of try or an error occurs.
        BufferedReader input;
        try {
            input = new BufferedReader(new FileReader(filename));
        } catch (IOException E) {
            System.out.println("Invalid filename: " + E.getMessage());
            return null;
        }
        String line;
        // See if reading is possible and get the initial input for the line for the while loop.
        try {
            line = input.readLine();
        } catch (IOException E) {
            System.out.println("Failed read: " + E.getMessage());
            return null;
        }
        ArrayList<String> parts = new ArrayList<String>();
        // Read the file line by line.
        while (line != null) {
            parts.addAll(Arrays.asList(line.split(","))); // Split it with every comma and add to the array, then read the next line.
            try {
                line = input.readLine();
            } catch (IOException E) {
                System.out.println("Failed read: " + E.getMessage());
                return null;
            }
        }
        // Close the file being read.
        try {
            input.close();
        } catch (IOException E) {
            System.out.println("Failed read: " + E.getMessage());
            return null;
        }
        return parts;
    }

    // Takes in ArrayList of Strings and adds valid ones to the date212 list member variable.
    /**
     * @param ArrayList<String> parts
     * Check the validity of the dates. If valid, add to relevant lists; if not, print to the console.
     */
    public static void check_validity_and_place(ArrayList<String> parts) {
        Date212 date;
        for (int i = 0; i < parts.size(); i++) {
            // Check if date is valid, if so, add to the date lists using the method of its class.
            date = new Date212(parts.get(i));
            if (date.get_is_valid()) {
                date212list.add(date);
                date212list_ordered.add(new Date212(parts.get(i)));
            } else {
                System.out.println("Invalid date: " + parts.get(i));
            }
        }
    }
}
