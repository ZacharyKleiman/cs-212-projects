/***************************************************************
 * 
 * @author Zachary Kleiman
 * Date212List class Project2. 
 * Creates 2 Date212 nodes, first and last
 */
public class Date212List {
    public Date212Node first;
    public Date212Node last;
    public int length;

    /**
     * Set everything to null in the constructor. Set the int length to 0
     */
    public Date212List() {
        // Default constructor if have no information
        first = null;
        last = null;
        length = 0;
    }

    /**
     * @param Date212 d
     * If the list is currently null, separate the case where first and last are the same node. 
     * Other cases will just append to the end of the list.
     */
    public void append(Date212 d) {
        // If the list is currently null, separate the case where first and last are the same node. Other cases will just append to the end of the list.
        if (first == null) {
            Date212Node n = new Date212Node(d);
            last = n;
            first = n;
            length++;
        } else {
            Date212Node n = new Date212Node(d);
            last.next = n;
            last = n;
            length++;
        }
    }
}
