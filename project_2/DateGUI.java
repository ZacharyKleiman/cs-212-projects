/***************************************************************
 * 
 * @author Zachary Kleiman
 * This is the GUI for the Date application
 */
import javax.swing.*;
import java.io.*;
import java.util.*;
import java.awt.*;
import java.util.ArrayList;

public class DateGUI extends JFrame {
    /**
     * @param UnsortedDate212List date212list
     * @param SortedDate212List date212list_ordered
     * 
     * This draws the GUI and places the unordered dates on the left column and the ordered ones on the right
     */
    public void initialize(UnsortedDate212List date212list, SortedDate212List date212list_ordered) {
        // Need to convert the ArrayList to an array
        // Make the GUI and set basic information
        DateGUI DateGUI1 = new DateGUI();
        DateGUI1.setSize(400, 200);
        DateGUI1.setLocation(100, 100);
        DateGUI1.setTitle("Dates");
        DateGUI1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        DateGUI1.setVisible(true);
        // Set Grid layout
        DateGUI1.setLayout(new GridLayout(1, 2));
        Container myContentPane = DateGUI1.getContentPane();
        // Set necessary stuff to display text
        TextArea Order_read = new TextArea();
        TextArea order_sorted = new TextArea();
        myContentPane.add(Order_read, BorderLayout.EAST);
        myContentPane.add(order_sorted, BorderLayout.WEST);
        myContentPane.add(Order_read);
        myContentPane.add(order_sorted);
        // Place the dates. Unordered is in the left column, and ordered is in the right column
        Date212Node t = date212list.first;
        while (t != null) {
            Order_read.append(t.d + "\n");
            t = t.next;
        }
        t = date212list_ordered.first;
        while (t != null) {
            order_sorted.append(t.d + "\n");
            t = t.next;
        }
    }
}
