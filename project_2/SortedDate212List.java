/***************************************************************
 * 
 * @author Zachary Kleiman
 * Make a sorted Date212list
 *
 */
public class SortedDate212List extends Date212List {
    /**
     * Use super to create the constructor since this extends Date212List
     */
    public SortedDate212List() {
        /*Date212Node ln = new Date212Node();
        first = ln;
        last = ln;
        length = 0;*/
        super();
    }

    /**
     * @param Date212 e
     * add it in a sorted order to the list
     */
    public void add(Date212 e) {
        // need to find an algorithm to get in proper position
        Date212Node temp = first;
        // if there's nothing in the list
        if (temp == null) {
            first = new Date212Node(e);
            last = first;
            length++;
            return;
        }
        // while no greater value move forward
        while (temp != null && temp.d.compareTo(e) < 0) {
            temp = temp.next;
        }
        // if no value afterwards add to end of list
        if (temp == null) {
            super.append(e);
        } else {
            // swap the value at temp with the date that we're adding
            // create a new node with the original date found in temp
            // set temp's next to the new node

            Date212 d2 = temp.d; // hold on to date
            temp.d = e;
            Date212Node node2 = new Date212Node(d2, temp.next);
            temp.next = node2;
            length++;
        }
    }
}
