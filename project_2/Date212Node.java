/***************************************************************
 * 
 * @author Zachary Kleiman
 * Date212Node class Project2. 
 * Creates a Date212 and a Date212Node object. Both of which are public
 */
public class Date212Node {
    // We're having a node which is like a C++ pointer to the next object
    protected Date212 d;
    protected Date212Node next;

    /**
     * @param Date212 d
     * @param Date212Node next
     * Sets the objects to those parameters
     */
    public Date212Node(Date212 d, Date212Node next) {
        this.d = d;
        this.next = next;
    } // Constructor

    /**
     * Default constructor
     */
    public Date212Node() {
        this.d = null;
        this.next = null;
    }

    /**
     * Constructor if we don't have a next node
     */
    public Date212Node(Date212 d) {
        this.d = d;
        this.next = null;
    }
}
