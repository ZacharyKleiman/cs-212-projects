/***************************************************************
 * 
 * @author Zachary Kleiman
 * Make an unsorted Date212list
 * This just uses methods defined in the super Date212List class
 */
public class UnsortedDate212List extends Date212List {
    // Refer to super
    public UnsortedDate212List() {
        super();
    }

    public void add(Date212 d) {
        super.append(d);
    }
}
