/***************************************************************
 * 
 * @author Zachary Kleiman
 * Date212 class. Used for Project1 and Project2. 
 * Date212 creates three private int instance variables day, month, year, and a boolean is_valid, and a date_saved string
 */
 
import javax.swing.*;
import java.io.*;
import java.util.*;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JTextArea;

public class Date212 {
    private int day = 0;
    private int month = 0;
    private int year = 0;
    private boolean is_valid;
    private String date_saved;

    /**
     * @param date Type string, date to parse for private instance variables.
     * Calls is_it_valid() to determine if the date is valid
     */
    public Date212(String date) {
        is_valid = true;
        if (date.length() != 8) {
            is_valid = false;
            return;
        }
        date_saved = date; 
        // Get the integer from the string in the proper format
        year = Integer.parseInt(date.substring(0, 4));
        month = Integer.parseInt(date.substring(4, 6));
        day = Integer.parseInt(date.substring(6, 8));
        is_valid = is_it_valid();
    }

    /**
     * @return Returns the value of is_valid which is private
     */
    public boolean get_is_valid() {
        return is_valid;
    }

    /**
     * Checks the validity of the date set in month, day, year.
     * @return False if an invalid date, true if a valid date
     */
    private boolean is_it_valid() {
        // Check if value between 1 and 12 for months
        if (month < 1 || month > 12) {
            return false; // Error
        }
        // Check validity of day
        if (day < 1 && day > 31) {
            return false; // Error
        }
        if (day > 30 && (month == 4 || month == 6 || month == 9 || month == 11)) {
            return false; // Error, invalid day of the month
        }
        if (day > 31 && (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)) {
            return false;
        }
        // Check February
        if (day > 28 && (month == 2)) {
            // Check if it's a leap year
            if (year % 4 == 0) {
                if (day > 29) {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * Create a toString method in class Date212 that will return the date
     * in the form mm/dd/yyyy. Use this method to display the dates in the GUI.
     * @return The string sorted properly
     */
    public String toString() {
        return date_saved.substring(4, 6) + "/" + date_saved.substring(6, 8) + "/" + date_saved.substring(0, 4);
    }

    /**
     * Compare this to another Date212 object to see which one comes first
     * -1 this is less
     * 1 this is more
     * 0 this is equal
     * @param Date212 Date1
     * @return -1 if the date being compared is greater than self, 0 if equal to self, 1 if less than self
     */
    public int compareTo(Date212 Date1) {
        // Compare year, if they're equal, then check the month, then the day. Only do the needed checks.
        if (this.year < Date1.year) {
            return -1;
        } else if (this.year > Date1.year) {
            return 1;
        } else {
            if (this.month < Date1.month) {
                return -1;
            } else if (this.month > Date1.month) {
                return 1;
            } else {
                if (this.day < Date1.day) {
                    return -1;
                } else if (this.day > Date1.day) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }
    }
}

