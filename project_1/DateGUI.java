import javax.swing.*;
import java.io.*;
import java.util.*;
import java.awt.*;
import java.util.ArrayList;
public class DateGUI extends JFrame {
	public void initialize(ArrayList<Date212> date212list, ArrayList<Date212> date212list_ordered) {
		//need to convert the ArrayList to an array
		Date212[] array_dates = new Date212[date212list.size()];
		array_dates = date212list.toArray(array_dates);
		Date212[] array_dates_ordered = new Date212[date212list_ordered.size()];
		array_dates_ordered = date212list_ordered.toArray(array_dates_ordered);
		//make the GUI and set basic informaiton
		DateGUI DateGUI1=new DateGUI();
		DateGUI1.setSize(400, 200);
		DateGUI1.setLocation(100, 100);
		DateGUI1.setTitle("Dates");
		DateGUI1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		DateGUI1.setVisible(true);
		//set Grid layout
		DateGUI1.setLayout(new GridLayout(1,2));
		Container myContentPane = DateGUI1.getContentPane();
		TextArea Order_read = new TextArea();
		TextArea order_sorted = new TextArea();
		myContentPane.add(Order_read, BorderLayout.EAST);
		myContentPane.add(order_sorted, BorderLayout.WEST);
		myContentPane.add(Order_read);
		myContentPane.add(order_sorted);
		//place the dates. Unordered is in the left column and ordered is in the right column
		for (int i=0;i < date212list.size();i++) {
			Order_read.append(array_dates[i]+"\n");
		}
		for (int i=0;i < date212list_ordered.size();i++) {
			order_sorted.append(array_dates_ordered[i]+"\n");
		}	
	}
}

