import javax.swing.*;
import java.io.*;
import java.util.*;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JTextArea;

public class Project1 {
	//declare the two arrays as belonging to the whole class so it's accessible to all methods
	private static ArrayList<Date212> date212list= new ArrayList<Date212>();
	private static ArrayList<Date212> date212list_ordered = new ArrayList<Date212>();
	public static void main(String args[]) {
		ArrayList<String> dates = new ArrayList<String>();
		try {dates = inputFromFile(args[0]);
		} catch (Exception e) {
			dates=inputFromFile("dates.txt");
		}
		// check dates isn't null and length isn't 0
		if (dates==null || dates.size()==0) {
			System.out.println("No dates in input file.");
			return;
		}
		check_validity_and_place(dates);
		selection_sort_date();
		/*for (int i=0;i<date212list.size();i++) {
			System.out.println(date212list.get(i).toString() + "\n");
		}	
		System.out.println("ordered");
		for (int i=0;i<date212list.size();i++) {
			System.out.println(date212list_ordered.get(i).toString() + "\n");
		}*/
		DateGUI gui_project1  = new DateGUI();
		gui_project1.initialize(date212list,date212list_ordered);		
	}
	public static ArrayList<String> inputFromFile(String filename)
	{
		//goal is to take input from file
		// output should be the list of dates read in
		//whether they're valid or not
		//Variables must be declared outside of try or an error occurs
		BufferedReader input; 
		try {
			input = new BufferedReader(new FileReader(filename)); 
		} catch(IOException E) {
			System.out.println("Invalid filename:" + E.getMessage());
			return null;
		}
		String line;
		//see if reading is possible and get initial input for line for the while loop
		try {
			line = input.readLine();
		} catch(IOException E) {
			System.out.println("Failed read:" + E.getMessage());
			return null;
		}
		ArrayList<String> parts = new ArrayList<String>();
		while (line!=null )
		{// while 
			parts.addAll(Arrays.asList(line.split(",")));
			//split it with every comma and add to the array then read next line
			try {
				line =input.readLine();
			} catch  (IOException E) {
				System.out.println("Failed read:" + E.getMessage());
			return null;
			}
		}
		//close the file being read
		try {
			input.close(); 
		}catch  (IOException E) {
			System.out.println("Failed read:" + E.getMessage());
			return null;
			}
		return parts;
	} // method inputFromFile
	// takes in Arraylist of Strings and adds valid ones to the date212 list member variable
	public static void check_validity_and_place(ArrayList<String> parts) {
		Date212 date;
		for (int i=0; i<parts.size();i++) {
			// check if date is valid, if so add to the date lists
			date = new Date212(parts.get(i));
			if (date.get_is_valid() == true) {
				date212list.add(date);
				date212list_ordered.add(date);
			} else {
				System.out.println( "Invalid date:" + parts.get(i));
			}
		}
	}
	public static void selection_sort_date() {
		// do selection sort of the ordered dates. 
		/*from GeeksforGeeks
		The algorithm works by maintaining two subarrays in a given array:
		The subarray is already sorted.
		Remaining subarray which is unsorted.
		In every iteration of the selection sort, the minimum element (considering ascending order) from the 	unsorted subarray is picked and moved to the sorted subarray.
		*/
		Date212 min;
		int min_index=0;
		for (int i=0;i<date212list_ordered.size();i++) {
			min = date212list_ordered.get(i);
			min_index=i;
			for (int j=i+1;j<date212list.size();j++) {
				if (min.compareTo(date212list_ordered.get(j)) > 0) {
					min = date212list_ordered.get(j);
					min_index=j;
				}
			}
			if (min_index!=i) {
				Collections.swap(date212list_ordered,i,min_index);
			}
		}
	}	
}
