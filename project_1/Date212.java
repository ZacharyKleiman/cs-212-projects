import javax.swing.*;
import java.io.*;
import java.util.*;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JTextArea;
/*
Create a class called Date212 to represent a date. It will store the year, month and day as integers (not
as a String), so you will need three private instance variables. A constructor should be provided that
takes a String (representing the date as above). Use the substring method of class String to pull out the
month, day and year, and parse them as integers. Include a private validity checking method that
should make sure that the month and day values are legal. If it is not a valid date, print it to the console
and do not put it in the array of dates. Create a toString method in class Date212 the will return the date
in the form mm/dd/yyyy. Use this method to display the dates in the GUI.
*/
class Date212 {
	private int day=0;
	private int month=0;
	private int year=0;
	private boolean is_valid;
	private String date_saved;
	public Date212 (String date) {
		is_valid=true;
		if (date.length()!=8) {
			is_valid = false;
			return;
		}
		date_saved = date; 
		year = Integer.parseInt(date.substring(0,4));
		month = Integer.parseInt(date.substring(4,6));
		day = Integer.parseInt(date.substring(6,8));
		is_valid=is_it_valid();
	}
	public boolean get_is_valid() {
		return is_valid;
	}
	private boolean is_it_valid () {
		//check if value between 1 and 12 for months
		if (month < 1 || month > 12) {
			return false; //error
		}
		// check validity of day
		if (day < 1 && day > 31) {
			return false; //error
		}
		if (day > 30 && (month==4 || month==6 || month ==9 || month==11)) {
			return false; //error, invalid day of month
		}
		if (day > 31 && (month==1 || month==3 || month ==5 || month==7 || month==8 || month==10|| month==12)) {
			return false;
		}
		//check february
		if (day > 28 && (month == 2)) {
			//check if it's a leap year
			if (year % 4 ==0) {
				if (day > 29) {
					return false;				
				}			
			}
			else {
				return false;
			}
		}
			return true;
	}
	//Create a toString method in class Date212 the will return the date
	//in the form mm/dd/yyyy. Use this method to display the dates in the GUI.
	public String toString () {
		return date_saved.substring(4,6) + "/" + date_saved.substring(6,8) + "/" + date_saved.substring(0,4);
	}
	//compare this to another Date212 object to see which one comes first
	public int compareTo(Date212 Date1) {
		if (this.year < Date1.year) {
			return -1;
		}
		else if (this.year > Date1.year) {
			return 1;
		} else {
			if (this.month < Date1.month) {
				return -1;
			}
			else if (this.month > Date1.month) {
				return 1;
			} else {
				if (this.day < Date1.day) {
					return -1;
				}
				else if (this.day > Date1.day) {
					return 1;
				} else {
					return 0;
				}
				}
				//return 0;
		}
	}
}
